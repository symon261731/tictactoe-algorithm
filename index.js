var testArrayFirstTableWin = [
    ['x', 'o', null],
    ['x', null, 'o'],
    ['x', 'o', 'o']
];
;
var testArraySecondTableWin = [
    ['x', 'o', null],
    ['o', 'o', 'x'],
    ['x', 'o', 'x']
];
var testArrayThirdTableWin = [
    ['x', null, 'x'],
    ['o', null, 'x'],
    ['x', null, 'x']
];
var testArrayRowFirstCircleWin = [
    ['o', 'o', 'o'],
    ['x', 'o', 'x'],
    ['x', 'x', null],
];
var testArrayRowSecondCrossWin = [
    ['o', 'x', 'x'],
    ['x', 'x', 'x'],
    ['o', 'o', null]
];
var testArrayRowThirdCircleWin = [
    ['o', 'x', 'x'],
    [null, null, 'o'],
    ['o', 'o', 'o']
];
var testArrayDiagonal = [
    ['o', 'x', 'x'],
    ['x', 'o', null],
    [null, 'x', 'o'],
];
var testArrayDiagonalReverse = [
    ['x', 'o', 'o'],
    ['x', 'o', 'x'],
    ['o', 'x', 'o']
];
var testArrayDraw = [
    ['o', 'x', 'o'],
    ['o', 'x', 'o'],
    ['x', 'o', 'x']
];
var testArrayNextMoveOne = [
    ['x', null, null],
    [null, null, null],
    ['x', null, 'o']
];
var testArrayNextMoveSecond = [
    ['x', null, 'o'],
    ['x', 'o', 'o'],
    [null, 'o', 'x']
];
function check(field) {
    var diagonal = [];
    var diagonalReverse = [field[0][2], field[1][1], field[2][0]];
    var draw = [];
    var table = [];
    var j = 0;
    for (var array = 0; array < field.length; array += 1) {
        if (field[array].every(function (rowValue) { return rowValue === 'x'; }))
            return 'крестики выиграли';
        if (field[array].every(function (rowValue) { return rowValue === 'o'; }))
            return 'нолики выиграли';
        for (var val = 0; val < field[array].length; val += 1) {
            table.push(field[val][j]);
            if (val === array) {
                diagonal.push(field[array][val]);
            }
            draw.push(field[array][val]);
        }
        if (table.every(function (el) { return el === 'x'; }))
            return 'крестики выиграли';
        if (table.every(function (el) { return el === 'o'; }))
            return 'нолики выиграли';
        table = [];
        j += 1;
    }
    if (diagonal.every(function (el) { return el === 'x'; }))
        return 'крестики выиграли';
    if (diagonal.every(function (el) { return el === 'o'; }))
        return 'нолики выиграли';
    if (diagonalReverse.every(function (el) { return el === 'x'; }))
        return 'крестики выиграли';
    if (diagonalReverse.every(function (el) { return el === 'o'; }))
        return 'нолики выиграли';
    if (draw.every(function (el) { return el !== null; }))
        return 'DRAW';
    return 'next move';
}
console.log('table', check(testArrayFirstTableWin), check(testArraySecondTableWin), check(testArrayThirdTableWin));
console.log('rows', check(testArrayRowFirstCircleWin), check(testArrayRowSecondCrossWin), check(testArrayRowThirdCircleWin));
console.log('diagonals', check(testArrayDiagonal), check(testArrayDiagonalReverse));
console.log('draw', check(testArrayDraw));
console.log('next=> ', check(testArrayNextMoveOne), check(testArrayNextMoveSecond));
