type ticTac = 'x' | 'o' | null;

const testArrayFirstTableWin: ticTac[][]= [
    ['x', 'o', null],
    ['x', null, 'o'],
    ['x', 'o', 'o']
];;

const testArraySecondTableWin: ticTac[][] = [
    ['x','o',null],
    ['o','o','x'],
    ['x','o','x']
];

const testArrayThirdTableWin: ticTac[][] = [
    ['x',null,'x'],
    ['o',null,'x'],
    ['x',null,'x']
]


const testArrayRowFirstCircleWin: ticTac[][] = [
    ['o','o','o'],
    ['x','o','x'],
    ['x','x',null],
]

const testArrayRowSecondCrossWin: ticTac[][] = [
    ['o', 'x', 'x'],
    ['x' ,'x', 'x'],
    ['o', 'o', null]
]
const testArrayRowThirdCircleWin: ticTac[][] = [
    ['o', 'x', 'x'],
    [null , null, 'o'],
    ['o' , 'o', 'o']
]



const testArrayDiagonal: ticTac[][] = [
    ['o','x','x'],
    ['x','o',null],
    [null,'x','o'],
]

const testArrayDiagonalReverse: ticTac[][] = [
    ['x','o','o'],
    ['x','o','x'],
    ['o','x','o']
]

const testArrayDraw: ticTac[][] =[
    ['o','x','o'],
    ['o','x','o'],
    ['x','o','x']
]

const testArrayNextMoveOne: ticTac[][] = [
    ['x',null,null],
    [null,null,null],
    ['x',null,'o']
]

const testArrayNextMoveSecond: ticTac[][] = [
    ['x', null,'o'],
    ['x', 'o', 'o'],
    [null,'o', 'x']
]



function check (field: Array<ticTac[]>){

    const diagonal = [];
    const diagonalReverse = [field[0][2],field[1][1],field[2][0]];
    const draw = [];
    let table = [];
    let j = 0
    for( let array= 0; array<field.length; array+=1) {

        if( field[array].every( rowValue => rowValue === 'x')) return 'крестики выиграли';
        if( field[array].every( rowValue => rowValue === 'o')) return 'нолики выиграли';

        for( let val=0; val<field[array].length; val+=1) {
            table.push(field[val][j])

            if(val === array){
                diagonal.push(field[array][val])
            }
            draw.push(field[array][val]);
        }
        if(table.every(el=> el==='x')) return 'крестики выиграли';
        if(table.every(el=> el==='o')) return 'нолики выиграли';
        table = [];
        j+=1
    }

    if( diagonal.every(el=>el==='x')) return 'крестики выиграли';
    if( diagonal.every(el=>el==='o')) return 'нолики выиграли';

    if( diagonalReverse.every(el=>el==='x')) return 'крестики выиграли';
    if( diagonalReverse.every(el=>el==='o')) return 'нолики выиграли';
    if( draw.every(el=> el!== null)) return 'DRAW'
    return 'next move';
}

console.log('table', check(testArrayFirstTableWin),check(testArraySecondTableWin),check(testArrayThirdTableWin));
console.log('rows',check(testArrayRowFirstCircleWin), check(testArrayRowSecondCrossWin) ,check(testArrayRowThirdCircleWin), );
console.log('diagonals',check(testArrayDiagonal), check(testArrayDiagonalReverse));
console.log('draw', check(testArrayDraw));
console.log('next=> ', check(testArrayNextMoveOne), check(testArrayNextMoveSecond));
